package com.the.sample.app.controller

import com.the.sample.app.model.User
import com.the.sample.app.service.UserService
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/users")
class UserRestController(val userService: UserService) {

    @GetMapping("/{id}")
    fun getUserById(@PathVariable("id") id: Long?): Mono<User?>? {
        return Mono.justOrEmpty(userService.findById(id!!))
    }

    @GetMapping("/{page}/{pageSize}")
    fun getAllUsers(
        @PathVariable("page") page: Int,
        @PathVariable("pageSize") pageSize: Int
    ): Flux<User?>? {
        return Flux.fromIterable(userService.findAll(page, pageSize))
    }

    @PostMapping("/")
    fun saverUser(@RequestBody user: User): Mono<User>? {
        userService.save(user)
        return Mono.just(user)
    }

    @PutMapping("/{id}")
    fun updateUser(@PathVariable("id") id: Long, @RequestBody user: User): Mono<User>? {
        user.id = id
        userService.save(user)
        return Mono.just(user)
    }

    @DeleteMapping("/{id}")
    fun deleteUser(@PathVariable("id") id: Long): Unit {
        userService.deleteById(id)
    }
}